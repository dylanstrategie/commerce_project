<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180831132418 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shopping_cart DROP FOREIGN KEY FK_72AAD4F67D59A6EF');
        $this->addSql('DROP INDEX IDX_72AAD4F67D59A6EF ON shopping_cart');
        $this->addSql('ALTER TABLE shopping_cart DROP linecarts_id');
        $this->addSql('ALTER TABLE line_cart ADD shopping_cart_id INT NOT NULL');
        $this->addSql('ALTER TABLE line_cart ADD CONSTRAINT FK_80EE7B6E45F80CD FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id)');
        $this->addSql('CREATE INDEX IDX_80EE7B6E45F80CD ON line_cart (shopping_cart_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE line_cart DROP FOREIGN KEY FK_80EE7B6E45F80CD');
        $this->addSql('DROP INDEX IDX_80EE7B6E45F80CD ON line_cart');
        $this->addSql('ALTER TABLE line_cart DROP shopping_cart_id');
        $this->addSql('ALTER TABLE shopping_cart ADD linecarts_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart ADD CONSTRAINT FK_72AAD4F67D59A6EF FOREIGN KEY (linecarts_id) REFERENCES line_cart (id)');
        $this->addSql('CREATE INDEX IDX_72AAD4F67D59A6EF ON shopping_cart (linecarts_id)');
    }
}
