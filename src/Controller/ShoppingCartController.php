<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ShoppingCartRepository;
use App\Entity\Article;
use App\Entity\LineCart;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/shopping_cart/{id}", name="shopping_cart")
     */
    public function index(ShoppingCartRepository $repo, int $id)
    {
        $shopping_cart = $repo->find($id);

        return $this->render('shopping_cart/index.html.twig', [
            'controller_name' => 'ShoppingCartController',
            'shopping_cart' => $shopping_cart,
        ]);
    }
}

 